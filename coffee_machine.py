class CoffeeMachine:

    def __init__(self):
        self.cups = 9
        self.water = 400
        self.milk = 540
        self.beans = 120
        self.money = 550

    def fill(self):
        self.water += int(input("Write how many ml of water do you want to add:\n"))
        self.milk += int(input("Write how many ml of milk do you want to add:\n"))
        self.beans += int(input("Write how many grams of coffee beans do you want to add:\n"))
        self.cups += int(input("Write how many disposable cups of coffee do you want to add:\n"))

    def take(self):
        print(f"I gave you ${self.money}")
        self.money = 0

    def check_resources(self, c_water, c_milk, c_beans):
        end_resource = ""
        if self.water - c_water < 0:
            end_resource = "water"
        elif self.milk - c_milk < 0:
            end_resource = "milk"
        elif self.beans - c_beans < 0:
            end_resource = "coffee beans"
        elif self.cups == 0:
            end_resource = "disposable cups"
        if end_resource == "":
            print("I have enough resources, making you a coffee!")
            return True
        else:
            print(f"Sorry, not enough {end_resource}!")
            return False

    def buy(self):
        coffee = input("What do you want to buy? 1 - espresso, 2 - latte, 3 - cappuccino, back - to main menu:\n")
        if coffee == "1":
            if self.check_resources(250, 0, 16):
                self.water -= 250
                self.beans -= 16
                self.money += 4
                self.cups -= 1
        elif coffee == "2":
            if self.check_resources(350, 75, 20):
                self.water -= 350
                self.milk -= 75
                self.beans -= 20
                self.money += 7
                self.cups -= 1
        elif coffee == "3":
            if self.check_resources(200, 100, 12):
                self.water -= 200
                self.milk -= 100
                self.beans -= 12
                self.money += 6
                self.cups -= 1
        elif coffee == "back":
            return

    def print_supp(self):
        print(f"""The coffee machine has:
    {self.water} of water
    {self.milk} of milk
    {self.beans} of coffee beans
    {self.cups} of disposable cups
    ${self.money} of money""")


def run_coffee():
    coffee = CoffeeMachine()
    while True:
        action = input("Write action (buy, fill, take, remaining, exit):\n")
        print()
        if action == "buy":
            coffee.buy()
        elif action == "fill":
            coffee.fill()
        elif action == "take":
            coffee.take()
        elif action == "remaining":
            coffee.print_supp()
        elif action == "exit":
            break
        else:
            print('A netyu')
        print()


run_coffee()
